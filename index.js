const express = require('express');
const app = express();

app.use((req, res, next) => {
    console.log(new Date());

    next();
});

app.use(express.static(__dirname + "/public"));

// Serving HTML-file
app.get("/", (req, res) => {
    res.sendFile("index.html");
});

// Handling API-request
app.get("/api", async (req, res) => {
    // ... Fetch data from database and send in response
    const dataFromDatabase = {
        breed: "pug",
        cute: true,
        age: 2,
    };
    
    res.send(dataFromDatabase);
});

app.listen(3000, () => {
    console.log("Server running on port " + 3000);
});